from sqlalchemy import create_engine
import plotly.express as px
import pandas as pd


if __name__ == "__main__":

    engine = create_engine('postgresql://analytic:7cnxmSPbe2Qmxpq5mxH5@data.engeto.com:5432/fch')

    df = pd.read_sql_query('select * from "container"', con=engine)

    fig = px.scatter_mapbox(df,
                            lat='lat',
                            lon='lon',
                            hover_name="title",
                            hover_data={"municipality", "street"},
                            color="origin",
                            color_discrete_map = {
                                'cervenekontejnery.cz': "red",
                                    'diakoniebroumov.cz': "blue",
                                    'potex.cz': "purple",
                                    'sbernedvory.cz': "grey",
                                    'textileco.cz:': "green"
                            },
                            zoom=6,
                            height=600
                            )
    fig.update_layout(mapbox_style="open-street-map")
    fig.show()



