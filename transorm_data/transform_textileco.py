import pandas as pd
from .insert_to_db import insert_to_db


def transform_textileco(textileco_array):

    df = pd.DataFrame(textileco_array)

    df['lat'] = df['lat'].astype(float)
    df['lon'] = df['lon'].astype(float)

    df['waste_types'] = 'textil'  # creating Type column
    df['origin'] = 'textileco.cz'  # adding origin of the source data
    df['land_register_nr'] = ''
    df['title'] = df['municipality'] + '-' + df['street']

    df.rename(
        columns={
            'street_number': 'house_nr',
            'title': 'title',
        },
        inplace=True
    )

    insert_to_db(df)
