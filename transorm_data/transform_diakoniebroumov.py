import pandas as pd
from .insert_to_db import insert_to_db


def transform_diakoniebroumov(db_array):

    df = pd.DataFrame(db_array)

    df['lat'] = df['lat'].astype(float)
    df['lon'] = df['lon'].astype(float)

    df['waste_types'] = 'texil'
    df['origin'] = 'diakoniebroumov.cz'
    df['region'] = ''
    df['land_register_nr'] = ''
    df['title'] = df['municipality'] + '-' + df['street']

    df.rename(
        columns={
            'street_number': 'house_nr',
            'title': 'title',
        },
        inplace=True
    )

    insert_to_db(df)
