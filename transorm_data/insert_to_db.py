from sqlalchemy import create_engine


def insert_to_db(df):

    result = df[[
        'correlationid',
        'title',
        'municipality',
        'house_nr',
        'land_register_nr',
        'street',
        'lat',
        'lon',
        'waste_types',
        'origin'
    ]]
    result.set_index("correlationid", inplace=True)
    # engine = create_engine('postgresql://pi:raspberry@192.168.0.178:5432/collection_containers')
    engine = create_engine('postgresql://analytic:7cnxmSPbe2Qmxpq5mxH5@data.engeto.com:5432/fch')

    result.to_sql('container', engine, if_exists="append")

    return result
