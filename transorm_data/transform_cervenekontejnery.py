import pandas as pd
import numpy as np
from .insert_to_db import insert_to_db


def transform_ck(ck_array):
    df = pd.DataFrame(ck_array)

    df['lat'] = df['Lat'].astype(float)
    df['lon'] = df['Long'].astype(float)
    df.fillna(value=np.nan, inplace=True)  # removing null values

    df['waste_types'] = 'electrical waste'  # adding column Waste_Types
    df['origin'] = 'cervenekontejnery.cz'  # adding origin of the source data
    df.rename(
        columns={
            'Internal_Number': 'correlationid',
            'mu_name': 'municipality',
            'Municipality': 'land_register_nr',
            'Name': 'title',
            'Street': 'street',
            'House_Nr': 'house_nr'
        },
        inplace=True
    )

    insert_to_db(df)
