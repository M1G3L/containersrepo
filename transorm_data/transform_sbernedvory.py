import pandas as pd
import numpy as np
from .insert_to_db import insert_to_db


def transform_sd(sd_array):
    df = pd.DataFrame(sd_array)

    df['lat'] = df['Lat'].astype(float)
    df['lon'] = df['Long'].astype(float)

    df = df.replace(r'^\s*$', np.nan, regex=True)  # removing white spaces / empty values
    df['origin'] = 'sberne-dvory.cz'  # adding origin of the source data
    df.rename(
        columns={
            'Internal_Number': 'correlationid',
            'mu_name': 'municipality',
            'Municipality': 'land_register_nr',
            'House_Nr': 'house_nr',
            'Land_Register_Nr': 'land_register_nr',
            'Waste_Types': 'waste_types',
            'Street': 'street',
            'Name': 'title'
        },
        inplace=True
    )

    insert_to_db(df)
