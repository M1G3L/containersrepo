import pandas as pd
from .insert_to_db import insert_to_db


def transform_potex(pt_array):

    df = pd.DataFrame(pt_array)

    df['lat'] = df['lat'].astype(float)
    df['lng'] = df['lng'].astype(float)

    df['waste_types'] = 'textil'  # creating Type column
    df['origin'] = 'potex.cz'  # adding origin of the source data
    df['house_nr'] = ''
    df['land_register_nr'] = ''

    df = df.drop(['distan'], axis=1)  # removing emtpy column
    df.rename(
        columns={
            'xid': 'correlationid',
            'city': 'municipality',
            'lat': 'lat',
            'lng': 'lon',
            'title': 'title',
            'address': 'street'
        },
        inplace=True)

    insert_to_db(df)
