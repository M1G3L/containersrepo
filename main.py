import extract_data as ed
import transorm_data as td

if __name__ == "__main__":

    # cervene kontejnery
    ck_array = ed.extract_ck()
    ck_data_frame = td.transform_ck(ck_array)

    print('Finished: cervenekontejnery')

    # potex
    pt_array = ed.extract_potex()
    pt_data_frame = td.transform_potex(pt_array)

    print('Finished: potex')

    # sberne dvory
    sd_array = ed.extract_sd()
    sd_data_frame = td.transform_sd(sd_array)

    print('Finished: sberne-dvory')

    # diakonie broumov
    db_array = ed.extract_db()
    pd_data_frame = td.transform_diakoniebroumov(db_array)

    print('Finished: diakoniebroumov')

    # textileco
    tc_array = ed.extract_textileco()
    td.transform_textileco(tc_array)

    print('Finished: textileco')
