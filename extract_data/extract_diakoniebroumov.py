import requests
from bs4 import BeautifulSoup
from .geoapify import get_geojson


def extract_db():

    db_url = 'http://www.diakoniebroumov.cz/cs/sbirkova-cinnost/sberne-kontejnery-v-cr'

    r = requests.get(url=db_url)

    # find all script tags, findAll returns array
    soup = BeautifulSoup(r.text, 'html.parser')

    data = soup.findAll('tr')
    del(data[0])  # remove description

    final = []
    index = 0
    prefix = 'db_'  # declaration of prefix for creation of correlationid

    # iterate containers
    for container_tag in data:

        info_tags = container_tag.findAll('td')

        container = {
            'municipality': info_tags[0].text,
            'street': info_tags[1].text,
            'street_number': info_tags[2].text
        }

        # extract latitude and longitude from API
        try:
            geojson = get_geojson(str(container['street']) + ' ' + str(container['street_number']))
            properties = geojson["features"][0]["properties"]

            container["lat"] = properties["lat"]
            container["lon"] = properties["lon"]
            container["correlationid"] = prefix+str(index)
            index += 1

            final.append(container)

        except:

            print("DB Failed: " + str(container))

    return final


if __name__ == "__main__":
    extract_db()
