import requests
import re
import json

api_key = '22d62c166a544b3fb128d942cf8b3f9d'


def get_geojson(search_text):

    url = 'https://api.geoapify.com/v1/geocode/search?text=' + search_text + '&apiKey=' + api_key

    r = requests.get(url=url)

    geojson = json.loads(r.content)

    # in case of failure split the 2 street intersection and try first
    if len(geojson["features"]) == 0:

        new_search_text = re.split(' x ', search_text)[0]
        new_search_text = re.split(r'[^\w\s:]', new_search_text)[0]

        if search_text == new_search_text:
            return geojson

        return get_geojson(new_search_text)

    return geojson

