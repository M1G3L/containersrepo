import requests
import re
from bs4 import BeautifulSoup
import json


def extract_ck():

    ck_url = "https://www.cervenekontejnery.cz/vyhledavani-na-mape/"

    r = requests.get(url=ck_url)

    soup = BeautifulSoup(r.text, 'html.parser')

    # find script tag with the array 'var data'
    pattern = re.compile(r"var data = \[\{.*\}.*\];")
    data = soup.find('script', text=pattern)

    # get the data from the content of the bs4 tag
    array_pattern = re.compile(r"\[.*\]")
    final = re.search(array_pattern, data.contents[0])

    # parse as array and remove last element
    final = json.loads(final.group())
    final = final[:-1]

    return final
