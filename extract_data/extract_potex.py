import requests
import re
from bs4 import BeautifulSoup
import json


def extract_potex():

    # potex containers
    potex_url = "https://www.recyklujemetextil.cz/kde-najdete-kontejnery/"

    r = requests.get(url=potex_url)

    soup = BeautifulSoup(r.text, 'html.parser')

    # find script tag with the array 'var places"
    pattern = re.compile(r".*var places.*")
    data = soup.find('script', text=pattern)

    # get the data from the content of the bs4 tag
    final = re.search(r"var places = \[.*\]", data.contents[0], re.DOTALL)
    # remove "var places ="
    final = final.group().replace("var places = ", "")
    final = json.loads(final)

    return final
