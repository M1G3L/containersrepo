import requests
import re
from bs4 import BeautifulSoup
import json


def extract_sd():

    # sberne dvory
    sd_url = "https://www.sberne-dvory.cz/vyhledavani/?search_by=region&show=list&page_listing=10" + \
                "&city=" + \
                "&street=" + \
                "&zip=" + \
                "&distance=60" + \
                "&limit=" + \
                "&region[]=CZ.jic" + \
                "&region[]=CZ.jmo" + \
                "&region[]=CZ.kav" + \
                "&region[]=CZ.krh" + \
                "&region[]=CZ.lib" + \
                "&region[]=CZ.mor" + \
                "&region[]=CZ.olo" + \
                "&region[]=CZ.par" + \
                "&region[]=CZ.plz" + \
                "&region[]=CZ.pra" + \
                "&region[]=CZ.stc" + \
                "&region[]=CZ.ust" + \
                "&region[]=CZ.vys" + \
                "&region[]=CZ.zli" + \
                "&page_listing=2000"

    r = requests.get(url=sd_url)

    # find all script tags, findAll returns array
    container_pattern = re.compile(r".*data.push.*")
    soup = BeautifulSoup(r.text, 'html.parser')
    data = soup.findAll('script', text=container_pattern)

    final = []

    # print(data)

    # get the data from the content of the bs4 tag
    for script_tag in data:

        container = script_tag.contents[0]
        container = container.replace("data.push(", "")
        container = container.strip()
        container = container[:-2]
        container = json.loads(container)
        container["Waste_Types"] = container["Waste_Types"].strip(',')

        final.append(container)

    return final
