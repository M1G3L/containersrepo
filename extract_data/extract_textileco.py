import requests
import re
from bs4 import BeautifulSoup
from .geoapify import get_geojson


def extract_textileco():

    textileco_url = 'https://www.textileco.cz/textileco'

    r = requests.get(url=textileco_url)

    # find all script tags, findAll returns array
    soup = BeautifulSoup(r.text, 'html.parser')

    data = soup.findAll('div', class_='tab-pane')

    final = []
    index = 0
    prefix = 'tc_'  # declaration of prefix for creation of correlationid

    # iterate regions
    for region_tag in data:

        if region_tag['id'] == 'i':
            continue

        region = region_tag.find('h4').text
        region_content = region_tag.findAll('ul')

        # content of region
        street_tags = region_content[0].findAll()
        municipality = ""

        # street and municipality name is on the same level
        for street_tag in street_tags:

            # get municipality
            if street_tag.name == 'strong':

                municipality = street_tag.text

            # get streets municipality
            elif street_tag.name == 'li':

                street = street_tag.text
                street = re.sub(r'\([^)]*\)', '', street)  # remove comments like '(u nemocnice)'
                street_str = street.split("x")[0].strip()  # split the street names if multiple
                street_str = re.sub(r'\([^)]*\)', '', street_str).strip()  # remove comments like '(u nemocnice)'
                street_number = re.search(r'[\d/-]+$', street_str)  # search for number, can include '670/7'

                if street_number:

                    street_name = ' '.join(street_str.split()[:-1])
                    street_number = street_number.group()

                else:

                    street_name = street_str
                    street_number = ""

                container = {
                    "region": region,
                    "municipality": municipality,
                    "street": street_name,
                    "street_number": street_number
                }

                # extract latitude and longitude from API
                try:
                    geojson = get_geojson(street)
                    properties = geojson["features"][0]["properties"]

                    container["lat"] = properties["lat"]
                    container["lon"] = properties["lon"]
                    container["correlationid"] = prefix+str(index)
                    index += 1

                    final.append(container)

                except:

                    print("TE Failed: " + str(container))

    return final


if __name__ == "__main__":
    extract_textileco()
